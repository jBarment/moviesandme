import { getTimeFieldValues } from "uuid-js";

const API_TOKEN = "815f4dd7683083d33e749723d8a74b3b";

export function getFilmsFromApiWithSearchedText (text) {
	const url = 'https://api.themoviedb.org/3/search/movie?api_key=' + API_TOKEN + '&language=fr&query=' + text
	return fetch(url)
	  .then((response) => response.json())
	  .catch((error) => console.error(error))
  }